/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-10-15
Programming Environment Used: Linux, gcc 9.3
*/

#include <iostream>
#include <string>
using namespace std;

class Invoice {
  public:
    Invoice()
      :partNumber{""}, 
      partDescription{""}, 
      pricePerItem{-1}, 
      quantity{-1}
    {}

    Invoice(string partNumber, string partDescription, int pricePerItem, int quantity)
      :partNumber{partNumber}, 
      partDescription{partDescription},
      pricePerItem{-1},
      quantity{-1}
    {
      setPricePerItem(pricePerItem);
      setQuantity(quantity);
    }
    
    string getPartNumber() {
      return partNumber;
    }
    void setPartNumber(string newPartNumber) {
      partNumber = newPartNumber;
    }
    string getPartDescription() {
      return partDescription;
    }
    void setPartDescription(string newPartDescription) {
      partDescription = newPartDescription;
    }
    int getPricePerItem() {
      return pricePerItem;
    }
    void setPricePerItem(int newPricePerItem) {
      if (newPricePerItem >= 0) {
        pricePerItem = newPricePerItem;
      } else {
        pricePerItem = 0;
        cout << "pricePerItem cannot be negative. pricePerItem set to " << pricePerItem << endl;
      }
    }
    int getQuantity() {
      return quantity;
    }
    void setQuantity(int newQuantity) {
      if (newQuantity >= 0) {
        quantity = newQuantity;
      } else {
        quantity = 0;
        cout << "quantity cannot be negative. quantity set to " << quantity << endl;
      }
    }
    int getInvoiceAmount(){
      return pricePerItem*quantity;
    }
    void printInvoice(){
      cout << "Part number: " << partNumber << endl;
      cout << "Part description: " << partDescription << endl;
      cout << "Quantity: " << quantity << endl;
      cout << "Price per item: $" << pricePerItem << endl;
      cout << "Invoice amount: $" << getInvoiceAmount() << endl;
    }

  private:
    string partNumber;
    string partDescription;
    int pricePerItem;
    int quantity;

};

int main() {
  Invoice B("12345","Hammer", 5, 100);
  B.printInvoice();

  B.setPartDescription("Saw");
  B.setPartNumber("123456");
  B.setPricePerItem(10);
  cout << "\n";
  B.setQuantity(-1);
  cout << "\nInvoice data members modified.\n" << endl;

  B.printInvoice();

  return 0;
}

