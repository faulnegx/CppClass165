/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-10-03
Programming Environment Used: Linux, gcc 9.3
*/

#include <iostream>
#include <string>
#include <cstdlib> // atof

using namespace std;

int main() {
  // Collect user input for wallHeights and wallWidth
  string buf;
  cout << "Please enter the percentage and I will tell you the letter grade: ";
  cin  >> buf;
  const double score = atof(buf.c_str());
  
  // Print Letter grade
  if (score > 100) {
    cout << "Error. " << score << " is not within 0 - 100." << endl;
  } else if (score >= 90) {
    cout << "Thank you. " << score << " is an A." << endl;
  } else if (score >= 80) {
    cout << "Thank you. " << score << " is a B." << endl;
  } else if (score >= 70) {
    cout << "Thank you. " << score << " is a C." << endl;
  } else if (score >= 60) {
    cout << "Thank you. " << score << " is a D." << endl;
  } else if (score >= 0) {
    cout << "Thank you. " << score << " is a F." << endl;
  } else {
    cout << "Error. " << score << " is not within 0 - 100." << endl;
  }
  return 0;
}

