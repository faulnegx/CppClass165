// tipping.cpp : This file contains the 'main' function. Program execution begins and ends there.//
#include <iostream>
#include <iomanip>
using namespace std;
int main(){
int serviceLevel = 0;
double totalBill = 0.0;
double liquor = 0.0;
double tipPercent = 0.0;
double totalNoLiquor = 0.0;
double tip = 0.0;//prompt user for total bill
cout << "Please enter bill total: ";
//save input
cin >> totalBill;
//prompt user for liquor amount
cout << "Enter the liquor charge: ";
//save input
cin >> liquor;
//prompt user for service satisfaction
cout << "How was your service?  Please rate:  \n1) EXCELLENT \n2) AVERAGE \n3) POOR\n" ;
//save input
cin >> serviceLevel;
//determine tip percent
if (serviceLevel == 2)
	tipPercent = .20;
else if (serviceLevel == 1)
tipPercent = .25;
else if (serviceLevel == 3)
tipPercent = .10;

//calculate total and tip
totalNoLiquor = totalBill - liquor;
tip = totalNoLiquor * tipPercent;
//output tip
cout << setprecision(2) << fixed << "Tip: $" << tip << endl;
return 0;
}