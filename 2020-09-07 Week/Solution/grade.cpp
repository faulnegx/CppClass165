#include <iostream>

using namespace std;

int main(){

	double percGrade = 0.0;

	cout << "Please enter the percentage grade: ";
	cin >> percGrade;

	if(percGrade >= 70 && percGrade < 80)
		cout << "Thank you. " << percGrade << " is a C.\n";
	else if( percGrade >= 80 && percGrade < 90)
		cout << "Thank you. " << percGrade << " is a B.\n";
	else if( percGrade >= 60 && percGrade < 70)
		cout << "Thank you. " << percGrade << " is D.\n";
	else if ( percGrade >= 90)
		cout << "Thank you. " << percGrade << " is an A.\n";
	else
	    cout << "Thank you. " << percGrade << " is an F.\n";

	cout << "Thank you for using my amazing program!" << endl;
	return 0;
}



