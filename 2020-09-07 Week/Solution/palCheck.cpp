#include <iostream>
 
int main(){
    //declare variables
    int number = 0, tenThou = 0, thou = 0, tens = 0, ones = 0;
    //user input
    std::cout << "Please enter a 5-digit integer or -1 to stop processing:  ";
    std::cin >> number;
    while(number != -1){
    while(number >= 100000 || number <= 9999){
      std::cout << "I'm sorry, but you must enter a 5-digit number.\n";
      std::cout << "Please try again.  Enter a 5-digit integer:  ";
      std::cin >> number;
    }//end while
        
        tenThou = number / 10000;
        thou = number % 10000 / 1000;
        tens = number % 100 / 10;
        ones = number % 10;
        //palindrome check
        //std::cout << tenThou << " " << thou << " " << tens << " " << ones << "\n";
        if(tenThou == ones && thou == tens)
            std::cout << number << " is a palindrome.\n";
        else
            std::cout << number << " is NOT a palindrome.\n";
   
          std::cout << "Please enter a 5-digit integer or -1 to stop processing:  ";
    std::cin >> number;
}//end while
    return 0;
}