/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-10-03
Programming Environment Used: Linux, gcc 9.3
*/

#include <iostream>
#include <string>
#include <cstdlib> // atof
#include <iomanip> // fixed, setprecision

using namespace std;

int main() {
  // Collect user input for total bill, liquor charge and service quality.
  string buf;
  cout << "Enter the total bill: ";
  cin  >> buf;
  const double totalBill = atof(buf.c_str());
  cout << "Enter the liquor charge: ";
  cin  >> buf;
  const double liquorCharge = atof(buf.c_str());
  cout << "Was your service 1) excellant 2) good 3) poor? ";
  cin  >> buf;
  double percent = 0.0;
  if (atoi(buf.c_str()) == 1) {
    percent = 0.2; 
  } else if (atoi(buf.c_str()) == 2) {
    percent = 0.15; 
  } else if (atoi(buf.c_str()) == 3) {
    percent = 0.1; 
  } else {
    cout << "Error: Service input must be 1, 2, or 3.";
    exit(1);
  }
  const double tips = (totalBill - liquorCharge) * percent;
  // Print tips
  cout << "Tip: $" << fixed << setprecision(2) << tips << endl;
  return 0;
}

