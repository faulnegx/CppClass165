/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-10-03
Programming Environment Used: Linux, gcc 9.3
*/

#include <iostream>
#include <string>
#include <cstdlib> // atof

using namespace std;

int main() {
  // Collect user input for wallHeights and wallWidth
  string buf;
  cout << "Please enter 5 digit integer: ";
  cin  >> buf;
  const int digits = atoi(buf.c_str());
  
  // Isolate digits
  const int ones = digits % 10;
  const int tens = (digits - ones) / 10 % 10;
  const int hundreds = (digits - ones - 10 * tens) / 100 % 10;
  const int thousands = (digits - ones - 10 * tens - 100 * hundreds) / 1000 % 10;
  const int tenThousands = (digits - ones - 10 * tens - 100 * hundreds - 1000 * thousands) / 10000 % 10;
  string is = "";
  if (ones == tenThousands && tens == thousands) {
    is = "";
  } else {
    is = " NOT";
  }
  cout << digits << " is" << is << " a palindrome.\n";
  return 0;
}

