/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-12-05
Programming Environment Used: Linux, gcc 9.3
*/


#include <iostream>
#include <string>

bool multiplesOf (int value, int divider) {
  return value % divider == 0;
}

int main() {
  // Functional approach example 
  for (int i = 1; i < 101; ++i) {
    bool newLine = false;
    if (multiplesOf(i, 3)) {
      std::cout << "Fizz";
      newLine = true;
    }
    if (multiplesOf(i, 5)) {
      std::cout << "Buzz";
      newLine = true;
    }
    if (newLine) {
      std::cout << std::endl;
    } else {
      std::cout << i << std::endl;
    }
  }
  return 0;
}
