/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-10-02
Programming Environment Used: Linux, gcc 9.3
*/

#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include <cstdlib> // atof

// minimum function take in 3 values of double type and return minimum 
double minimum(const double a,const double b,const double c) {
  const double firstMin = (a < b) ? a : b;
  return (firstMin < c) ? firstMin : c;
}

// maximum function take in 3 values of double type and return maximum 
double maximum(const double a,const double b,const double c) {
  const double firstMax = (a > b) ? a : b;
  return (firstMax > c) ? firstMax : c;
}

int main() {
  const int numInputs = 3;
  vector<double> values(numInputs, 0.0);
  string buf;
  // Collect user input for values
  for (size_t i = 0; i < values.size(); ++i) {
    cout << "Enter value " << i + 1 << ":" << endl;
    cin >> buf;
    values.at(i) = atof(buf.c_str());
  }
  // Report input values as interpreted by atof
  cout << "You entered: ";
  for (double value : values) {
    cout << value << " ";
  }
  cout << endl;
  // calculate maximum and minimum of the values.
  cout << "Maximum of your values is " 
       << maximum(values.at(0), values.at(1), values.at(2))
       << endl;
  cout << "Minimum of your values is " 
       << minimum(values.at(0), values.at(1), values.at(2))
       << endl;
  return 0;
}

