/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-12-05
Programming Environment Used: Linux, gcc 9.3
*/


#include "Search.hpp"

namespace Search {

size_t binarySearchPresorted(int value, std::vector<int> &sortedVector) {
  // return SIZE_MAX when value not found
  size_t lowIndex = 0;
  size_t highIndex = sortedVector.size()-1;
  size_t middleIndex = 0;

  while (highIndex >= lowIndex) {
    middleIndex = lowIndex + (highIndex - lowIndex) / 2;
    if (value == sortedVector.at(middleIndex)) {
      return middleIndex;
    } else if (value > sortedVector.at(middleIndex)) {
      lowIndex = middleIndex + 1;
    } else if (value < sortedVector.at(middleIndex)) {
      highIndex = middleIndex - 1;
    }
  }
  return SIZE_MAX;
}

size_t linearSearch(int value, std::vector<int> &v, size_t minIndex, size_t maxIndex) {
  // return SIZE_MAX when value not found
  for (size_t index = minIndex; index <= maxIndex; ++index) {
    if (value == v[index]) {
      return index;
    }
  }
  return SIZE_MAX;
}

}