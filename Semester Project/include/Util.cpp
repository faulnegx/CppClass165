/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-12-05
Programming Environment Used: Linux, gcc 9.3
*/
#include "Util.hpp"

namespace Util {

void randomIntFillVector(std::vector<int> &vec, int min, int max, bool repeatOkay) {
  // Random seed must be set by others
  // Vector to be filled with postive integer between min and max (inclusive)
  if (min > max || min < 0 || max > RAND_MAX || (!repeatOkay && (vec.size() > static_cast<size_t>(max-min+1)))) {
    // exit filling vector with zeroes. These cannot be handled by this function.
    for (int& ele: vec) {
      ele = 0;
    }
    std::cout << "randomIntFillVector Failed.";
    return;
  }
  int range = max - min + 1;
  for (int& ele: vec) {
    ele = rand() % range + min;
  }
  if (!repeatOkay) {
    for (size_t i = 1; i < vec.size(); ++i) {
      size_t found = Search::linearSearch(vec[i], vec, 0, i - 1);
      bool repeated = (found != SIZE_MAX);
      while (repeated) {
        vec[i] = rand() % range + min;
        found = Search::linearSearch(vec[i], vec, 0, i - 1);
        repeated = (found != SIZE_MAX);
      }
    }
  }
}

void printVector(const std::vector<int> &v) {
  for (const int& ele: v) {
    std::cout << ele << " ";
  }
  std::cout << std::endl;
}

bool inVector(const int val, const std::vector<int> &v) {
  for (auto &ele : v) {
    if (ele == val) {
      return true;
    }
  }
  return false;
}

}