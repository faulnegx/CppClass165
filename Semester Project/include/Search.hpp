/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-12-05
Programming Environment Used: Linux, gcc 9.3
*/
#ifndef SEARCH_HPP
#define SEARCH_HPP

#include <bits/c++config.h>
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>

namespace Search {

size_t binarySearchPresorted(int value, std::vector<int> &sortedVector);
size_t linearSearch(int value, std::vector<int> &v, size_t minIndex, size_t maxIndex);

}
#endif
