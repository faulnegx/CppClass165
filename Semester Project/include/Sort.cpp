/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-12-05
Programming Environment Used: Linux, gcc 9.3
*/


#include "Sort.hpp"

namespace Sort {


void selectionSort(std::vector<int> &vec) {
  // Starting with 0th index, go through the vector and select the minimum value. 
  // Swap that with the front of the vector. Now the front of the vector is the smallest.
  // Repeat the process until the end
  for (size_t index = 0; index < vec.size(); ++index) {
    size_t minIndex = index;
    for (size_t selectorIndex = index+1; selectorIndex < vec.size(); ++selectorIndex) {
      if (vec[selectorIndex] < vec[minIndex]) {
        minIndex = selectorIndex;
      }
    }
    if (minIndex != index) {
      std::swap(vec[index], vec[minIndex]);
    }
  }
}

void insertionSort(std::vector<int> &vec) {
  // 0th index by itself will be sorted.
  // Start with 1th index, swap to the left until 0 through index is in increasing order
  // Repeat with the next index until the very end of vec.
  for (size_t i = 1; i < vec.size(); ++i) {
    size_t thisElementIndex = i;
    while (thisElementIndex > 0 && vec[thisElementIndex] < vec[thisElementIndex-1]) {
      std::swap(vec[thisElementIndex], vec[thisElementIndex-1]);
      thisElementIndex = thisElementIndex-1;
    }
  }
}

void mergeSortRecursive(
  std::vector<int> &v, std::vector<int>::iterator beginIt, std::vector<int>::iterator endIt) {
    if (endIt - beginIt == 1) {
      return;
    } else {
      std::vector<int>::iterator::difference_type difference = endIt - beginIt;
      // Make a copy of iterator to make a middle iterator 
      std::vector<int>::iterator middleIt(beginIt);
      std::advance(middleIt, difference/2);
      // Divide
      mergeSortRecursive(v, beginIt, middleIt); // Sort leftHalf
      mergeSortRecursive(v, middleIt, endIt); // Sort rightHalf

      // combine 2 sorted halves
      std::vector<int>::iterator leftIt(beginIt);
      std::vector<int>::iterator rightIt(middleIt);
      std::vector<int> tempSortedValues (static_cast<size_t>(difference), 0);
      std::vector<int>::iterator tempIt = tempSortedValues.begin();
      while (tempIt != tempSortedValues.end()) {
        // Copy the lower value (between leftHalf and righthalf) into tempSortedValues.
        if (*leftIt < *rightIt) {
          *tempIt = *leftIt;
          ++leftIt;
        } else {
          *tempIt = *rightIt;
          ++rightIt;
        }
        ++tempIt;

        if (leftIt == middleIt) {
          // Copied all values from leftHalf. No need to move remaining rightHalf to 
          // tempSortedValues since they don't have to move at all.
          tempIt = tempSortedValues.end();
        } else if (rightIt == endIt) {
          // Copied all values from rightHalf. Move the whole rest of leftHalf without 
          // checking.
          for (;leftIt != middleIt; ++leftIt) {
            *tempIt = *leftIt;
            ++tempIt;
          }
        }
      }
      // put tempSortedValues into v
      tempIt = tempSortedValues.begin();
      for (std::vector<int>::iterator it(beginIt);it != rightIt; ++it) {
        *it = *tempIt;
        ++tempIt;
      }
    }
  }

void mergeSort(std::vector<int> &vec) {
  mergeSortRecursive(vec, vec.begin(), vec.end());
}


void bubbleSort(std::vector<int> &vec) {
  // Swap current value with the next if current is larger. Repeat until finalIndex.
  // Now the finalIndex holds the largest value. 
  // Repeat this process from the lastIndex of vec down to index 1. 
  // Then, the whole vec will be sorted.
  size_t index = 0;
  for (size_t finalIndex = vec.size()-1; finalIndex > index; --finalIndex) {
    for (; index < finalIndex; ++index) {
      if (vec[index] > vec[index+1]) {
        std::swap(vec[index], vec[index+1]);
      }
    }
    index = 0;
  }
}


void quickSortRecursive(
  std::vector<int> &v, std::vector<int>::iterator beginIt, std::vector<int>::iterator endIt) {
    if (endIt - beginIt <= 1) {
      // std::cout << "Base case" << std::endl;
      return;
    } else {
      // Paritition
      // Always use front of the vector as pivot for now. Copy pivotvalue;
      // TODO: we should use a random value to get average case 
      int pivotValue = *beginIt;
      // gapIt divides left/right array. Value is discardable because it is already 
      // copied to pivotValue.
      std::vector<int>::iterator gapIt(beginIt);

      // Option 1(implemented): put the right array in the end of the array and swap the
      // end of the array up to the front (we could be unnecessarily moving the stuff at
      // the end of the array that doesn't need to be moved)
      // Option 2: put the right array in temporary storage and put back at the end 
      // (additional memory cost and copy there and copy back cost)

      // TODO: there should be a way to find matching left and right to swap them. That
      // would minimize swap numbers.
      std::vector<int>::iterator leftIt(beginIt);
      ++leftIt;
      std::vector<int>::iterator rightIt(endIt);
      --rightIt;
      while (leftIt <= rightIt) {
        if (*leftIt < pivotValue) {
          std::swap(*leftIt, *gapIt);
          ++gapIt;
          ++leftIt;
        } else {
          std::swap(*leftIt, *rightIt);
          --rightIt;
        }
      }

      // rightIt is the always the correct final location for pivotValue
      std::swap(pivotValue, *rightIt);

      quickSortRecursive(v, beginIt, gapIt); // Sort leftHalf
      quickSortRecursive(v, std::next(gapIt), endIt); // Sort rightHalf
    }
}

void quickSort(std::vector<int> &vec) {
  quickSortRecursive(vec, vec.begin(), vec.end());
}

void reverseVector(std::vector<int> &vec) {
  // It would probably be better if the sorting algorithm can sort in reverse. 
  // This adds vec.size()/2 number of memory swap
  for (size_t index = 0; index < vec.size()/2; ++index) {
    std::swap(vec[index], vec[vec.size()-1-index]);
  }
}

}