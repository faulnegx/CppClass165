/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-12-05
Programming Environment Used: Linux, gcc 9.3
*/


#ifndef SORT_HPP
#define SORT_HPP

#include "Util.hpp"
#include <cstdlib> // RAND_MAX
#include <algorithm> // iter_swap
#include <iostream>
#include <iterator> // advance, next, prev
#include <utility> // swap
#include <vector>

namespace Sort {
  void selectionSort(std::vector<int> &vec);
  void insertionSort(std::vector<int> &vec);
  void mergeSortRecursive(
    std::vector<int> &v, std::vector<int>::iterator beginIt, std::vector<int>::iterator endIt);
  void mergeSort(std::vector<int> &vec);
  void bubbleSort(std::vector<int> &vec);
  void reverseVector(std::vector<int> &vec);
  void quickSortRecursive(
    std::vector<int> &v, std::vector<int>::iterator beginIt, std::vector<int>::iterator endIt);
  void quickSort(std::vector<int> &vec);
}
#endif
