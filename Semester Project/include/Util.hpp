/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-12-05
Programming Environment Used: Linux, gcc 9.3
*/

#ifndef UTIL_HPP
#define UTIL_HPP
#include "Search.hpp"
#include <iostream>
#include <vector>
#include <string>

namespace Util {
void randomIntFillVector(std::vector<int> &vec, int min = 0, int max = RAND_MAX, bool repeatOkay = false);
void printVector(const std::vector<int> &v);
bool inVector(const int val, const std::vector<int> &v);
}
#endif
