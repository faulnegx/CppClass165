/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-12-05
Programming Environment Used: Linux, gcc 9.3
*/
#include "Search.hpp"
#include "Sort.hpp"
#include "Util.hpp"
#include <iostream>
#include <string>
#include <iterator>
#include <vector>

class LotteryMachine {
  public: 
    LotteryMachine() {
    }
    std::vector<int> getWinningNumbers() const {
      return winningNumbers;
    }
    static void introduction() {
      std::cout << "\nWelcome to CPP Lottery Machine!\n"
      "Lottery machine will generate a list of unique non-negative integers. \n"
      "You may determine the difficulty of the lottery by specifying \n"
      "how many numbers are generated and the inclusive range of integers. \n"
      "Press Ctrl-C to exit" << std::endl;
    }
    void setLottery() {
      std::cout << "\nPlease enter how many numbers will the lottery machine generate?" << std::endl;
      std::string buff;
      std::cin >> buff;
      length = atoi(buff.c_str());
      // Need to catch if length provide is negative number and more than 1.
      std::cout << "Lottery machine will generate a set of lottery " << length;
      std::cout << " long." << std::endl;
      winningNumbers.resize(static_cast<size_t>(length));
      userNumbers.resize(static_cast<size_t>(length));

      std::cout << "Please enter the minimum possible non-negative integer for lottery?" << std::endl;
      std::cin >> buff;
      min = atoi(buff.c_str());
      // Need to catch if length provide is negative number.

      std::cout << "Please enter the maximum possible non-negative integer for lottery?" << std::endl;
      std::cin >> buff;
      max = atoi(buff.c_str());
      // Need to catch if length provide is negative number.
    }
    void genLottery(){
      Util::randomIntFillVector(winningNumbers, min, max, duplicatesAllow);
      Sort::mergeSort(winningNumbers);
    }

    void setUserNumbers() {
      std::string buff;
      userNumbers.clear();
      while (userNumbers.size() < winningNumbers.size()) {
        std::cout << "Please enter your guess " << std::endl;
        std::cin >> buff;
        int userInputInt = atoi(buff.c_str());
        if (userInputInt < 0) {
          std::cout << "Input is negative. Please try again" << std::endl;
        } else if (userInputInt < min) {
          std::cout << "Input is smaller than minimum possible integer."
          " Please try again" << std::endl;
        } else if (userInputInt > max) {
          std::cout << "Input is larger than maximum possible integer."
          " Please try again" << std::endl;
        } else if (!duplicatesAllow && Util::inVector(userInputInt, userNumbers)) {
          std::cout << "Duplicates not allowed and input already entered."
          " Please try again" << std::endl;
        } else {
          userNumbers.push_back(userInputInt);
          // insertionSort is fast when the array is mostly sorted already.
          Sort::insertionSort(userNumbers);
        }
      }
    }
    void printState() {
      std::cout << "Lottery machine state: " << std::endl;
      std::cout << "winningNumbers: ";
      Util::printVector(winningNumbers);
      std::cout << "userNumbers: ";
      Util::printVector(userNumbers);
      std::cout << "length: " << length << std::endl;
      std::cout << "min: " << min << std::endl;
      std::cout << "max: " << max << std::endl;
    }
    void printResults() const {
      std::cout << "The winning numbers are ";;
      Util::printVector(winningNumbers);
      std::cout << std::endl;
      std::cout << "Thank you for playing. Hope to see you again soon!" << std::endl;
    }
  private: 
    std::vector<int> winningNumbers{};
    std::vector<int> userNumbers{};
    bool duplicatesAllow = false;
    int length = 10;
    int min = 0;
    int max = 100;

};

int main() {
  srand(time(0));
  std::cout << "\nState: We can print statements from static class methods without creating an instance." << std::endl;
  LotteryMachine::introduction();
  std::cout << "\nState: LotteryMachine is created with default values." << std::endl;
  LotteryMachine lm = LotteryMachine();
  lm.printState();
  lm.setLottery();
  std::cout << "\nState: LotteryMachine is updated with user inputs." << std::endl;
  lm.printState();
  lm.genLottery();
  std::cout << "\nState: LotteryMachine has generated winning numbers." << std::endl;
  lm.printState();
  lm.setUserNumbers();
  std::cout << "\nState: LotteryMachine has stored userNumbers." << std::endl;
  lm.printState();

  std::cout << std::endl;
  lm.printResults();
  return 0;
}
