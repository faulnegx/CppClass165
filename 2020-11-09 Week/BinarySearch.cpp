/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-11-15
Programming Environment Used: Linux, gcc 9.3
*/

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

int binary_search(int rangeLow, int rangeHigh) {
  int middleValue = rangeLow + (rangeHigh - rangeLow)/2;
  cout << "My guess is " << middleValue << endl;
  cout << "Please tell me if your number is higher, lower, or equal to my guess: ";
  string buf;
  cin >> buf;
  cout << endl;

  int correct_answer{-1};
  if (buf == "lower") {
    correct_answer = binary_search(rangeLow, middleValue);
    return correct_answer;
  } else if (buf == "higher") {
    correct_answer = binary_search(middleValue+1, rangeHigh);
    return correct_answer;
  } else if (buf == "equal") {
    correct_answer = middleValue;
    return correct_answer;
  } else {
    cout << "Your input was not higher, lower or equal. Please try again." << endl;
    correct_answer = binary_search(rangeLow, rangeHigh);
    return correct_answer;
  }
}

int main() {
  cout << "Guess the number game begins!" << endl;
  cout << "Now you pick random number and a range(inclusive) for me to guess." << endl;
  cout << "Minimum: ";
  string buf;
  cin >> buf;
  int rangeMin = atoi(buf.c_str());
  cout << "Maximum: ";
  cin >> buf;
  int rangeMax = atoi(buf.c_str());
  cout << endl;
  int userNum = binary_search(rangeMin, rangeMax);
  cout << "Your number must be " << userNum << ". Thanks for playing!" << endl;

  return 0;
}

