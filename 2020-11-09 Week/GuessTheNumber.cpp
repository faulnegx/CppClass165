/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-11-15
Programming Environment Used: Linux, gcc 9.3
*/

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

class Guess_the_number
{
  public:
    Guess_the_number();
    Guess_the_number(int rangeMax);

    int guess(int guessInt);
    void human_guesser();
    void computer_guesser();
  private:
    int answer;
};

Guess_the_number::Guess_the_number (int rangeMax):
  answer{-1}
  {
    srand(time(0));
    answer = rand() % rangeMax + 1;
    cout << "Game set up for guessing between 0 and " << rangeMax << endl;
    cout << answer << endl;
  }
int Guess_the_number::guess (int guessInt)
{
  if (answer == guessInt) {
    cout << "Your guess is FINALLY correct. What took you so long?" << endl;
    return 0;
  } else if (answer > guessInt) {
    cout << "Try guessing higher." << endl;
    return 1;
  } else {
    cout << "Try guessing lower." << endl;
    return -1;
  } 
}

void Guess_the_number::human_guesser()
  {
    string guessStr;
    int guessInt;
    int guessCorrect = 2;
    // safety feature no more than 10 iterations
    int maxIterations = 10;
    int iterations = 0;
    // 
    while (guessCorrect != 0) {
      cout << "Enter your next guess:" << endl;
      cin >> guessStr;
      guessInt = atoi(guessStr.c_str());
      guessCorrect = guess(guessInt);
      // limit max iterations
      iterations++;
      if (iterations >= maxIterations && guessCorrect != 0) {
        cout << "Max iterations " << maxIterations << " reached. Game end!" << endl;
        break;
      };
    };
  }

int binary_search(int rangeLow, int rangeHigh) {
  int middleValue = rangeLow + (rangeHigh - rangeLow)/2;
  cout << "My guess is " << middleValue << endl;
  cout << "Please tell me if your number is higher, lower, or equal to my guess: ";
  string buf;
  cin >> buf;

  int correct_answer{-1};
  if (buf == "lower") {
    correct_answer = binary_search(rangeLow, middleValue);
    return correct_answer;
  } else if (buf == "higher") {
    correct_answer = binary_search(middleValue+1, rangeHigh);
    return correct_answer;
  } else if (buf == "equal") {
    correct_answer = middleValue;
    return correct_answer;
  } else {
    cout << "Your input was not higher, lower or equal. Please try again." << endl;
    correct_answer = binary_search(rangeLow, rangeHigh);
    return correct_answer;
  }
}

int main() {
  // Guess number
  cout << "Guess the number!" << endl;
  // Create a game
  // Guess_the_number Game1(10);
  // Game1.human_guesser();
  // Guess_the_number Game2(100);

  cout << "Now you pick a number from 0 - 10" << endl;
  int userNum = binary_search(0, 10);
  cout << "Your number must be " << userNum << endl;

  return 0;
}

