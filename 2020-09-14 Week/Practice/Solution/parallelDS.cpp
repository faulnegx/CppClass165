// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <iomanip>

using namespace std;
int main()
{	const int COUNT = 5;
	double averageScore = 0.0, totalScoreTest1 = 0.0, totalScoreTest2 = 0.0, totalScoreTest3 = 0.0;
	string lName[COUNT] = { "Brown", "Wu", "Hill", "Lang", "Black" };
	double test1[COUNT] =    {  34.0, 96.0, 82.0, 93.0, 91.0 };
	double test2[COUNT] =    {  69.0, 25.0, 96.0, 36.0, 14.0 };
	double test3[COUNT] =    {  12.0, 53.0, 27.0, 39.0, 15.0 };

	//output all student's averages across all three tests
	cout << "\n\n";
	for (int i = 0; i < 5; i++) {
		cout << setprecision(2) << fixed << "\n" << lName[i] << "'s information: " << "\t" << "Test 1: " << test1[i] 
		<< "\t" << "Test 2:  " << test2[i] << "\t" << "Test 3:  " << test3[i] << "\t" 
		<< "Average of 3 tests: "  << (( test1[i] + test2[i] + test3[i]) / 3.0);
	}
	cout << "\n";

	cout << endl;
	return 0;
}

