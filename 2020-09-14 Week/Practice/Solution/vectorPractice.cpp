#include <iostream>
#include <vector>
using namespace std;

int main() {
   const int NUM_STUDENTS = 3;
   vector<double> testScores(NUM_STUDENTS );
   int i;
   double totalScore;
   double averageScore;
   double maxScore;
   
   for (i = 0; i < NUM_STUDENTS ; ++i) {
      cout << "Enter Score " << i + 1 << ":" << endl;
      cin >> testScores.at(i);
   }
   
   cout << "You entered: ";
   for (i = 0; i < NUM_STUDENTS ; ++i) {
      cout << testScores.at(i) << " ";
   }
   cout << endl;
   
   totalScore = 0;
   cout << endl << "Total Score: ";
   for (i = 0; i < NUM_STUDENTS ; ++i) {
      totalScore += testScores.at(i);
   }
   cout << totalScore << endl;
   
   averageScore = totalScore/NUM_STUDENTS ;
   cout << "Average Score: " << averageScore << endl;
   
   maxScore = 0;
   for (i = 0; i < NUM_STUDENTS ; ++i) {
      if (testScores.at(i) > maxScore) {
         maxScore = testScores.at(i);
      }
   }
   cout << "Max Score: " << maxScore << endl;
   
   return 0;
}