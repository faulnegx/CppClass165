/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-09-17
Programming Environment Used: Linux, gcc 9.3
*/

#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include <cstdlib> // atof

int main() {
  const int numPlayers = 12;
  vector<double> weights(numPlayers, 0.0);
  string buf;
  // Collect user input for weights
  for (size_t i = 0; i < weights.size(); ++i) {
    cout << "Enter weight " << i + 1 << ":" << endl;
    cin >> buf;
    weights.at(i) = atof(buf.c_str());
  }
  // output weights entered
  cout << "You entered: ";
  for (double weight : weights) {
    cout << weight << " ";
  }
  cout << "\n" << endl;

  // calculate total weights and determine max weight
  double totalWeight = 0.0;
  double maxWeight = weights.at(0);
  for (double weight : weights) {
    // Update totalWeight
    totalWeight += weight;
    // Update maxWeight if this weight is larger than maxWeight
    if (weight > maxWeight) {
      maxWeight = weight;
    }
  }
  // calculate average Weight
  const double averageWeight = totalWeight / numPlayers;

  // Output answers to screen
  cout << "Total weight: " << totalWeight << endl;
  cout << "Average weight: " << averageWeight << endl;
  cout << "Max weight: " << maxWeight << endl;

  return 0;
}

