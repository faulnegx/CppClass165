/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-09-17
Programming Environment Used: Linux, gcc 9.3
*/

#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include <cstdlib> // atof

int main() {
  const int howmany = 10;
  double scores[howmany] = {};
  string buf;
  // Collect user input for weights
  for (int i = 0; i < howmany; ++i) {
    cout << "Enter score " << i + 1 << ":" << endl;
    cin >> buf;
    scores[i] = atof(buf.c_str());
  }
  // output weights entered
  cout << "You entered: ";
  for (int i = 0; i < howmany; ++i) {
    cout << scores[i] << " ";
  }
  cout << "\n" << endl;

  // // calculate total weights and determine max weight
  double totalScores = 0.0;
  double maxScore = scores[0];
  for (int i = 0; i < howmany; ++i) {
    // Update totalWeight
    totalScores += scores[i];
    // Update maxWeight if this weight is larger than maxWeight
    if (scores[i] > maxScore) {
      maxScore = scores[i];
    }
  }
  //calculate average Weight
  const double averageScore = totalScores / howmany;

  // Output answers to screen
  cout << "Total Scores: " << totalScores << endl;
  cout << "Average score: " << averageScore << endl;
  cout << "Max score: " << maxScore << endl;

  return 0;
}

