/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-10-15
Programming Environment Used: Linux, gcc 9.3
*/

#include <iostream>
#include <string> // getline
#include <vector>
using namespace std;

#include <cstdlib> // atoi

int main() {
  string input;
  getline(cin, input);
  // find first space then atoi up to and not including space
  size_t leftSpacePos = 0;
  size_t rightSpacePos = input.find(" ");
  const string wordsCountStr = input.substr(leftSpacePos, rightSpacePos - 0);
  // wordCountStr must always be a positive number and be smaller than INT_MAX
  const size_t wordsCount = static_cast<size_t>(atoi(wordsCountStr.c_str()));

  // extract words from input to wordsList
  leftSpacePos = rightSpacePos;
  rightSpacePos = input.find(" ", leftSpacePos + 1);
  vector<string> wordsList;
  for (size_t i = 0; i < wordsCount; ++i) {
    wordsList.push_back(input.substr(leftSpacePos + 1, rightSpacePos - leftSpacePos - 1));
    leftSpacePos = rightSpacePos;
    rightSpacePos = input.find(" ", leftSpacePos + 1);
  }

  // extract lookUpChar from input
  const char lookUpChar = input.at(leftSpacePos + 1);
  for (string ele : wordsList) {
    if (ele.find(lookUpChar) != string::npos) {
      cout << ele << endl;
    }
  }

  return 0;
}

