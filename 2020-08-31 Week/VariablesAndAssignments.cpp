/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-165-5065
Date: 2020-09-03
Programming Environment Used: Linux, gcc 9.3
*/

#include <iostream>
#include <string>
using namespace std;

#include <cmath> // ceil
#include <cstdlib> // atof

int main() {
  // Set constant
  const double SF_PER_GALLON = 350.0;
  // Collect user input for wallHeights and wallWidth
  string buf;
  cout << "Enter wall height (feet): ";
  cin  >> buf;
  const double wallHeight = atof(buf.c_str());
  cout << "Enter wall width (feet): ";
  cin  >> buf;
  const double  wallWidth = atof(buf.c_str());

  // Do calculations
  const double wallArea = wallWidth * wallHeight;
  const double paintNeeded = wallArea / SF_PER_GALLON;
  const double cansNeeded = ceil(paintNeeded);

  // Output answers to screen
  cout << "Wall area: " << wallArea << " square feet" << endl;
  cout << "Paint needed: " << paintNeeded << " gallons" << endl;
  cout << "Cans needed: " << cansNeeded << " can(s)" << endl;

  return 0;
}

