#include <iostream>
#include <fstream>
#include <ctime>
// using std::time;

#include <string>
#include <cstdio>
// using std::printf;
using namespace std;

int main() {
  // set rand's seed by sending it current time?
  srand(time(nullptr));
  int answerKey[10]{};
  int q1_1 = 0;
  int q1_2 = 0;
  for (int i = 1; i < 11; ++i) {
    // generate random numbers
    q1_1 = rand() % 100 + 1;
    q1_2 = rand() % 100 + 1;
    answerKey[i-1] = q1_1 % q1_2;
    cout << i << ") What is " << q1_1 << " % " << q1_2 << " ?" << endl;
  }
  cout << "\nANSWER KEY" << endl;

  // print out answer key
  for (int i = 1; i < 11; ++i) {
    cout << i << ") " << answerKey[i-1] << endl;
  }


  // learn how to output to file
  ofstream outfile;
  outfile.open("answerkey.txt");
  // Check for error
  if (outfile.fail()) {
    cerr << "Error opening file" << endl;
    exit(1);
  }
  // Read a file until you've reached the end
  for (int i = 0; i < 10; ++i) {
    outfile << i+1 << ") " << answerKey[i] << "\n";
  }
  outfile.close();
  cout << "Finish writing to answerkey.txt" << endl;

  return 0;
}

