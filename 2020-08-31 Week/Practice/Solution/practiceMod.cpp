#include <iostream>
#include <cstdlib>
#include <ctime>    
using namespace std;

int main() { 
   // rand() returns a non-negative integer no greater than RAND_MAX
   // RAND_MAX is a constant whose default value may vary between implementations but it is 
   //          guraenteed to be at least 32767.
   // srand()  initializes rand()
   // time(0) returns the number of seconds since Jan 1 1970
  srand(time(0));  
  int operand1 = 0;
  int operand2 = 0;
  int answer1 = 0, answer2 = 0, answer3 = 0, answer4 = 0, answer5 = 0, answer6 = 0, answer7 = 0,
   answer8 = 0, answer9 = 0, answer10 = 0;

  operand1 = 1 + rand() % 100;
  operand2 = 1 + rand() % 100;
  cout << "1) What is " << operand1 << " % " << operand2 << "?\n";
  answer1 = operand1 % operand2;
 
  operand1 = 1 + rand() % 100;
  operand2 = 1 + rand() % 100;  
  cout << "2) What is " << operand1 << " % " << operand2 << "?\n";
  answer2 = operand1 % operand2;

  operand1 = 1 + rand() % 100;
  operand2 = 1 + rand() % 100;
  cout << "3) What is " << operand1 << " % " << operand2 << "?\n";
  answer3 = operand1 % operand2;

  operand1 = 1 + rand() % 100;
  operand2 = 1 + rand() % 100;
  cout << "4) What is " << operand1 << " % " << operand2 << "?\n";
  answer4 = operand1 % operand2;

  operand1 = 1 + rand() % 100;
  operand2 = 1 + rand() % 100;
  cout << "5) What is " << operand1 << " % " << operand2 << "?\n";
  answer5 = operand1 % operand2;

  operand1 = 1 + rand() % 100;
  operand2 = 1 + rand() % 100;
  cout << "6) What is " << operand1 << " % " << operand2 << "?\n";
  answer6 = operand1 % operand2;

  operand1 = 1 + rand() % 100;
  operand2 = 1 + rand() % 100;
  cout << "7) What is " << operand1 << " % " << operand2 << "?\n";
  answer7 = operand1 % operand2;

  operand1 = 1 + rand() % 100;
  operand2 = 1 + rand() % 100;
  cout << "8) What is " << operand1 << " % " << operand2 << "?\n";
  answer8 = operand1 % operand2;

  operand1 = 1 + rand() % 100;
  operand2 = 1 + rand() % 100;
  cout << "9) What is " << operand1 << " % " << operand2 << "?\n";
  answer9 = operand1 % operand2;

  operand1 = 1 + rand() % 100;
  operand2 = 1 + rand() % 100;
  cout << "10) What is " << operand1 << " % " << operand2 << "?\n";
  answer10 = operand1 % operand2;
   

  cout << "\nANSWER KEY\n";
  cout << "1)\t" << answer1 << "\n";
  cout << "2)\t" << answer2 << "\n";
  cout << "3)\t" << answer3 << "\n";
  cout << "4)\t" << answer4 << "\n";
  cout << "5)\t" << answer5 << "\n";
  cout << "6)\t" << answer6 << "\n";
  cout << "7)\t" << answer7 << "\n";
  cout << "8)\t" << answer8 << "\n";
  cout << "9)\t" << answer9 << "\n";
  cout << "10)\t" << answer10 << endl;









   return 0;
}