#include <iostream>
#include <cstdlib>
#include <ctime>    
using namespace std;

int main() { 
   // rand() returns a non-negative integer no greater than RAND_MAX
   // RAND_MAX is a constant whose default value may vary between implementations but it is 
   //          guraenteed to be at least 32767.
   // srand()  initializes rand()
   // time(0) returns the number of seconds since Jan 1 1970
   srand(time(0));  

   cout << "a) ";
   for(int i = 1; i <= 20; i++)
      cout << 1 + rand() % 6 <<  " ";
   cout << endl << endl;
   
   cout << "b) ";
   for(int i = 1; i <= 20; i++)
      cout << 2 * ( rand() % 9 + 1 ) <<  " ";
   cout << endl << endl;
   
   cout << "c) ";
   for(int i = 1; i <= 20; i++)
     cout << ( 1 + rand() % 2 )    << " ";
   cout << endl << endl;
   
   cout << "d) ";
   for(int i = 1; i <= 20; i++)
     cout << ( 1 + rand() % 100 )    << " ";
   cout << endl << endl;

   cout << "e) ";
   for(int i = 1; i <= 20; i++)
     cout << ( 1 + 2 * ( 1 + rand() % 5 ) )    << " ";
   cout << endl << endl;

   cout << "f) ";
   for(int i = 1; i <= 20; i++)
     cout << rand() % 10    << " ";
   cout << endl << endl;

   cout << "g) ";
   for(int i = 1; i <= 20; i++)
     cout << rand() % 3 - 1    << " ";
   cout << endl << endl;

   cout << "h) ";
   for(int i = 1; i <= 20; i++)
     cout << 6 + 4 * ( rand() % 5 )    << " ";
   cout << endl << endl;

   cout << "i) ";
   for(int i = 1; i <= 20; i++)
     cout <<  rand() % 15 - 3  << " ";
   cout << endl << endl;
   return 0;
}