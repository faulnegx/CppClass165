#include <iostream>
#include <fstream>
#include <ctime>
// using std::time;

#include <string>
#include <cstdio>
// using std::printf;
using namespace std;

int main() {
  // set rand's seed by sending it current time?
  srand(time(nullptr));
  int set[]{1, 2, 3 ,4 ,5 ,6};
  int setSize = (int) (sizeof(set)/sizeof(set[0]));
  cout << "setSize is " << setSize << "\n";
  // pick random item in a set
  for (int i = 0; i < setSize; ++i) {
    // generate random numbers
    int randIndex = rand() % (setSize);
    cout << "Random item in set is: " << set[randIndex] << endl;
  }
  cout << "\n";
  cout << "rand() % 6 + 1 is " << rand() % 6 + 1 << endl;
  cout << "2 * (rand() % 8) + 2 is " << 2 * (rand() % 9) + 2 << endl;
  cout << "rand() % 2 + 1 is " << rand() % 2 + 1 << endl;
  cout << "rand() % 100 + 1 is " << rand() % 100 + 1 << endl;
  cout << "rand() % 10 is " << rand() % 10 << endl;
  cout << "rand() % 3 - 1 is " << rand() % 3 - 1 << endl;
  cout << "4 * (rand() % 5) + 6 is " << 4 * (rand() % 5) + 6 << endl;
  cout << "rand() % 15 - 3 is " << rand() % 15 - 3 << endl;
  return 0;
}

